### 三勾知识付费系统、支持多端发布，一套代码发布到8个平台，面向开发，方便二次开发




### 项目介绍


三勾知识付费系统基于thinkphp8+element-plus+uniapp打造的面向开发的知识付费系统，方便二次开发或直接使用，可发布到多端，包括微信小程序、微信公众号、QQ小程序、支付宝小程序、字节跳动小程序、百度小程序、android端、ios端。


### 软件架构

后端：thinkphp8 管理端页面：element-plus 小程序端：uniapp。

部署环境建议：Linux + Nginx + PHP8.1 + MySQL5.7，上手建议直接用宝塔集成环境。

### 技术特点
- 前后分离 (分工协助 开发效率高)
- 统一权限 (前后端一致的权限管理)
- uniapp (一套代码8个平台，开发不浪费)
- thinkphp8(上手简单，极易开发)
- element-plus(饿了么前端开源管理后台框架，方便快速开发)

 ### 安装教程、开发文档、操作手册请进入官网查询

[官网链接](https://www.jjjshop.net)

[安装部署文档](https://doc.jjjshop.net/edu)


### 项目演示 

官网地址：https://www.jjjshop.net/      

后台演示：https://edu-test.jjjshop.net/admin     账号密码：admin/123456

 ### 扫码体验微信小程序，更多演示请扫码公众号查看 
![输入图片说明](https://www.jjjshop.net/gitee/edu/demo.png "demo.png")


 ### 如果对您有帮助，您可以点右上角 "Star" 支持一下，这样我们才有继续免费下去的动力，谢谢！
QQ交流群 (入群前，请在网页右上角点 "Star" )

交流QQ群：305595727

 ### bug反馈

如果你发现了bug，请发送邮件到 bug@jiujiujia.net，我们将及时修复并更新。 

 ### 特别鸣谢 
- thinkphp:[https://www.thinkphp.cn](https://www.thinkphp.cn)
- element-plus:[https://element-plus.gitee.io/zh-CN/](https://element-plus.gitee.io/zh-CN/)
- vue:[https://cn.vuejs.org/](https://cn.vuejs.org/)
- easywechat:[https://www.easywechat.com/](https://www.easywechat.com/)

 ### 小程序截图
![输入图片说明](https://www.jjjshop.net/gitee/edu/app-1.png "app-1.png")
![输入图片说明](https://www.jjjshop.net/gitee/edu/app-2.png "app-2.png")


 ### 后台截图 
![输入图片说明](https://www.jjjshop.net/gitee/edu/shop-1.png "shop-1.png")
![输入图片说明](https://www.jjjshop.net/gitee/edu/shop-2.png "shop-2.png")
![输入图片说明](https://www.jjjshop.net/gitee/edu/shop-3.png "shop-3.png")
![输入图片说明](https://www.jjjshop.net/gitee/edu/shop-4.png "shop-4.png")
![输入图片说明](https://www.jjjshop.net/gitee/edu/shop-5.png "shop-5.png")

 ### saas端截图 

![输入图片说明](https://www.jjjshop.net/gitee/edu/saas-1.png "saas-1.png")
![输入图片说明](https://www.jjjshop.net/gitee/edu/saas-2.png "saas-2.png")
 